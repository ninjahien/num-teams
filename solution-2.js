/************************
 * n = rating.length
 * 1 <= n <= 200
 * 1 <= rating[i] <= 10^5
 * O(n^2)
 ************************/

var constraints = function(rating) {
    var n = rating.length;
    return (n >= 1 && n <= 200) && rating.every(function(value) {
        return value >= 1 && value <= Math.pow(10, 5);
    });
}

var numTeams = function(rating) {
    var n = rating.length;
    var output = 0;
    var ltArr = Array.from({ length: n }, (_, idx) => 0);
    var gtArr = Array.from({ length: n }, (_, idx) => 0);
    for(var i = 0; i < n; i++) {
        for(var j = i+1; j < n; j++) {
            if (rating[i]<rating[j]) ltArr[j]++;
            else if (rating[i]>rating[j]) gtArr[j]++;
        }
    }
    for(var i = 0; i < n; i++) {
        for(var j = i+1; j < n; j++) {
            if (rating[i]<rating[j]) output+=ltArr[i];
            else if (rating[i]>rating[j]) output+=gtArr[i];
        }
    }
    return output;
};

// run & check constraints
var main = function(rating) {
    if (!constraints) return; // to check the constraints
    console.log('The number of team: ', numTeams(rating));
}