/************************
 * n = rating.length
 * 1 <= n <= 200
 * 1 <= rating[i] <= 10^5
 * O(n * log (10^5))
 ************************/

var constraints = function(rating) {
    var n = rating.length;
    return (n >= 1 && n <= 200) && rating.every(function(value) {
        return value >= 1 && value <= Math.pow(10, 5);
    });
}

var BinaryIndexedTree = (function () {
    function BinaryIndexedTree(n) {
        this.ft = (function (s) { var a = []; while (s-- > 0)
            a.push(0); return a; })(n + 1);
    }
    BinaryIndexedTree.prototype.bsum = function (r) {
        var sum = 0;
        for (; r > 0; r -= (r & (-r))) {
            sum += this.ft[r];
        }
        return sum;
    };
    BinaryIndexedTree.prototype.update = function (k, v) {
        for (; k < this.ft.length; k += (k & (-k))) {
            this.ft[k] += v;
        }
    };
    return BinaryIndexedTree;
}());

var solve = function (rating, n) {
    var bit1 = new BinaryIndexedTree(n);
    var bit2 = new BinaryIndexedTree(n);
    var sum = 0;
    for (var i = 0; i < rating.length; i++) {
        sum += bit1.bsum(rating[i] - 1);
        bit1.update(rating[i], bit2.bsum(rating[i] - 1));
        bit2.update(rating[i], 1);
    }
    return sum;
};

var reverse = function (rating) {
    var i = 0;
    var j = rating.length - 1;
    while (i < j) {
        var temp = rating[i];
        rating[i] = rating[j];
        rating[j] = temp;
        i++;
        j--;
    }
};

var numTeams = function (rating) {
    var cnt = 0;
    var maxR = 0;
    for (var index = 0; index < rating.length; index++) {
        var r = rating[index];
        {
            maxR = Math.max(maxR, r);
        }
    }
    cnt += solve(rating, maxR);
    reverse(rating);
    cnt += solve(rating, maxR);
    return cnt;
};

// run & check constraints
var main = function(rating) {
    if (!constraints) return; // to check the constraints
    console.log('The number of team: ', numTeams(rating));
}